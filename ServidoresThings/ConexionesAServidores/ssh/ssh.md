**Conexion Con SSH **

        #ssh -p //Puerto //User@//Ip
            (*) Parametros 
                (*) -p 


    *** Trasferencia De Archivos Al Servidor ***

    (*) Transferir archivos hacia el servidor remoto
        
        #scp archivo-en-local.tar user@1.2.3.4:/directorio/destino/servidor/remoto


    (*) Transferir archivos desde el servidor remoto
    
        #scp user@1.2.3.4:/ruta/servidor/remoto/archivo.tgz archivo-en-local.tgz



    ** Información Sacada De **

    https://www.arsys.es/blog/utilizar-comando-scp

