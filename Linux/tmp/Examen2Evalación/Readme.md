	
# Primer Ejercicio:
#### Crea la siguiente estructura en /home/[xxxx] donde [xxxx] es tu usuario (recuerda que el sistema operativo es Linux) (3 ptos.)



##### Forma Rapida
		
		Sudo  cd ~/SI && apt install tree -y

	cd ~/ && mkdir SI && mkdir SI/DocumentosPropios SI/Practicas SI/ConTROles && mkdir SI/DocumentosPropios/Libros SI/DocumentosPropios/Apuntes && mkdir SI/Practicas/ejercicios && mkdir SI/ConTROles/Comun && mkdir SI/ConTROles/Comun/usuarioX

##### Forma Lenta

	cd ~/ 
	mkdir SI
		cd SI
			mkdir DocumentosPropios Practicas ConTROles
				cd DocumentosPropios 
					mkdir Libros Apuntes
					cd ..

				cd Practicas 
					mkdir ejercicios
					cd ..

				cd ConTROles
					mkdir Comun
						cd Comun
							mkdir usuarioX
	cd ~/

#### 1:	 Con el comando tree muestra una imagen del resultado en la que aparezca esta parte del árbol de directorios

	tree SI/

#### 2: Posicionado en la carpeta Libros, copia los archivos cuyo nombre comience por “c” del directorio /bin al directorio usuarioX

##### Forma Rapida:

	cd ~/SI/DocumentosPropios/Libros &&  cp -r  /bin/c* ~/SI/ConTROles/Comun/usuarioX && ls  ~/SI/ConTROles/Comun/usuarioX

##### Forma Lenta:

	cd ~/SI
	cd DocumentosPropios/Libros
		cp -r  /bin/c* ~/SI/ConTROles/Comun/usuarioX
		ls  ~/SI/ConTROles/Comun/usuarioX

#### 3: Concatena los archivos /etc/passwd y /etc/shadow en un solo archivo llamado concatenado.exa

##### Forma Rapida:

	¿Donde? xd
		
	 Si es donde En el Directorio de SI/ConTROles/Comun/usuarioX Asi:
	 cd ~/SI/ConTROles/Comun/usuarioX && echo "Passwd:" >> concatenado.exa &&  cat  /etc/passwd >>  concatenado.exa && echo "shadown:" >> concatenado.exa && sudo  cat /etc/shadow >>  concatenado.exa

	 Si es donde en el Directorio de Si/DocumentosPropios/Libros Asi:
	 cd ~/SI/ConTROles/Comun/usuarioX && echo "Passwd:" >> concatenado.exa &&  cat  /etc/passwd >>  concatenado.exa && echo "shadown:" >> concatenado.exa && sudo  cat /etc/shadow >>  concatenado.exa


##### Forma Lenta:
	
	¿Donde? xd
		
	 Si es donde En el Directorio de SI/ConTROles/Comun/usuarioX Asi:
	 	
	 cd ~/SI/ConTROles/Comun/usuarioX
	 cat  /etc/passwd >>  concatenado.exa && sudo  cat /etc/shadow >>  concatenado.exa


	 Si es donde en el Directorio de Si/DocumentosPropios/Libros Asi:

	 cd ~/SI/DocumentosPropios/libros
	 cat  /etc/passwd >>  concatenado.exa && sudo  cat /etc/shadow >>  concatenado.exa
		
#### 4: Copia el archivo /etc/fstab en el directorio Practicas y llámalo montar.exa. Enseña el contenido de montar.exa

##### Forma Rapida:

	cd ~/SI/Practicas && cp /etc/fstab montar.exa && cat montar.exa

##### Forma Lenta:

	cd ~/SI/Practicas 
		cp /etc/fstab montar.exa 
			cat montar.exa


#### 5:  En el directorio Comun crea un archivo vacío llamado one1. A continuación, crea un enlace duro sobre one1 llamado one_d en la carpeta ejercicios y un enlace simbólico sobre one1 llamado one_s en la carpeta Libros.

##### Forma Rapida:
			
	cd ~/SI/ConTROles/Comun && touch one1 && ln one1 one_d &&  mv one_d ../../Practicas/ejercicios/ && ../../Practicas/ejercicios/ &&  ln -s one1 one_s &&  mv one_s ../../DocumentosPropios/Libros/ && ls ../../DocumentosPropios/Libros/
	
##### Forma Lenta:

	cd ~/SI/ConTROles/Comun
	 touch one1
	 ln one1 one_d	
    mv one_d ../../Practicas/ejercicios/ && ../../Practicas/ejercicios/ 

	ln -s one1 one_s
	m v one_s ../../DocumentosPropios/Libros/ && ls ../../DocumentosPropios/Libros/

##### Comandos Ultilizados

	ls 
	mkdir
	cd 
		cd ~/
	cp 
		-r --> 
	ln
	ln -s
	cat
	>
	>>

# Ejercicio Script:

##### Enunciado:

	En el primer caso debe pedir por pantalla el nombre de un directorio.
	Si el directorio existe, debe mostrar un mensaje de que ya existe. En caso contrario, debe crearlo e 	indicar que el directorio ha sido creado. 
	En el segundo caso debe pedir un fichero y obtener el patrón indicado. Probar la 	opción 2 con el fichero /etc/passwd 

	Se evaluará:
	Aparición del menú en pantalla limpia y centrado (0,5 ptos.)
	Funcionamiento de la opción de búsqueda de un directorio (0,5 ptos.) 
	Funcionamiento de la opción de obtención de texto de un archivo (0,5 ptos.) 
	Comprobación del resultado con etc/passwd (0,5 ptos.) 
	Control de errores en menú (0,5 ptos.) 
	Creación de ejecutable que permita su ejecución directa. (0,5 ptos.)


##### Terminal:

		// Creamos el Archivo vacio
		touch examen.sh
		
		// para Editar el archivo usar nano o vim 
			vim examen.sh


		//  Para darle  permiso de ejecución al script 
			chmod +x examen.sh

		// Para ejecutarlo
			./examen.sh

##### Script:
	#!/bin/dash

	echo " " 
	echo "\t\t\t\t******"
	echo "\t\t\t\t MENÚ"
	echo "\t\t\t\t******"
	echo " "

	echo "1.\t  Comprobación de la existencia de un directorio"
	echo "2.\t  Obtención de primera y tercera  columna de un archivo"

	read Numero 
	case $Numero in

	1)

		echo "  El nombre de un  Directorio  "
	read Ruta 

	if [  -d  $Ruta ]; then
		echo "El Directorio ya Existe"
	else
		mkdir $Ruta 
		echo "El Directorio ha sido Creado " 
		ls
	fi	 
		;;
	2)
			read -p "Dime el fichero Indicado:  "  fichero
			cat $fichero | awk -F ":" '{print $1 "\t" $3 }' 
	;;
	*) echo "$Numero No es  Opción no valida"
		;;
	esac

