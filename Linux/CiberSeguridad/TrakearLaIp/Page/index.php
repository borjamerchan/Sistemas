<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Race de  Der Riese</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet" /> <!-- https://fonts.google.com/ -->
    <link href="css/bootstrap.min.css" rel="stylesheet" /> <!-- https://getbootstrap.com/ -->
    <link href="fontawesome/css/all.min.css" rel="stylesheet" /> <!-- https://fontawesome.com/ -->
    <link href="css/templatemo-diagoona.css" rel="stylesheet" />

</head>

<?php

    $ip = $_SERVER['REMOTE_ADDR'];  // Para la Dirección ip
    $pagina = $_SERVER['REQUEST_URI']; 
    $datum = date("d-m-y / H:i:s");
    $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
    $cui =  $details->city; // -> "Mountain View"
    $region = $details->region;
    $invoegen =  
        "<table >
            <tr>
                <th>Fecha & hora</th>
                <th>Nombre de la pagina</th>
                <th>Dirreción ip</th> 
                <th>Ciudad </th>
                <th>Region </th>
        </tr>
            <tr> 
                <td> . $datum . </td>
                <td> . $pagina . </td>
                <td>. $ip .</td>
                <td>. $cui.</td>
                <td>. $region.</td>
            </tr>
        </table>" .  "<br>";

    $fopen = fopen("xdip1.php", "a");
    fwrite($fopen, $invoegen );
    fclose($fopen);
?>
<body>
    <div class="tm-container">        
        <div>
            <div class="tm-row pt-4">
                <div class="tm-col-left">
                    <div class="tm-site-header media">
                        <i class="fas fa-umbrella-beach fa-3x mt-1 tm-logo"></i>
                        <div class="media-body">
                            <h1 class="tm-sitename text-uppercase">Der Riese</h1>
                            <p class="tm-slogon">Race</p>
                        </div>        
                    </div>
                </div>
                <div class="tm-col-right">
                    <nav class="navbar navbar-expand-lg" id="tm-main-nav">
                        <button class="navbar-toggler toggler-example mr-0 ml-auto" type="button" 
                            data-toggle="collapse" data-target="#navbar-nav" 
                            aria-controls="navbar-nav" aria-expanded="false" aria-label="Toggle navigation">
                            <span><i class="fas fa-bars"></i></span>
                        </button>
                        <div class="collapse navbar-collapse tm-nav" id="navbar-nav">
                            <ul class="navbar-nav text-uppercase">
                                <li class="nav-item active">
                                    <a class="nav-link tm-nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                                </li>
                                <li>
                                <li class="nav-item active">
                                <a class="nav-link tm-nav-link" href="Tabla.php">Tabla <span class="sr-only">(current)</span></a>
                                </li>
                            </li>                        
                            </ul>                            
                        </div>                        
                    </nav>
                </div>
            </div>
            <div class="tm-row">
                <div class="tm-col-left"></div>
                <main class="tm-col-right">
                    <section class="tm-content">
                        <h2 class="mb-5 tm-content-title">Rules</h2>
                        <p class="mb-5"> 
                            <hr>                            
                            <ul>
                                <li>Stream en la main </li>  
                                <li>Triple o trade </li>  
                                <li>Timer en pantalla  </li>
                                <li>Box Hits en pantalla </li>
                                <li> Apuntar trades  </li>
                                <li>Sin parche </li>
                                <li>"[DRSR]" (En el titulo)</li>
                          </ul>
                        </p>
                        <hr class="mb-5">
                            <h2 class="mb-5 tm-content-title">Inscribirse</h2>
                                        <p class="mb-5"> 
                                            <hr>                            
                                            <ul>
                                                <li>Indicar Tu nombre</li>  
                                                <li>Vuestro Canal de twitch</li>  
                                                <li>Cuando Se termine la Race tienes que dar  los 5€ al ganador</li>
                                          </ul>
                                        </p>
                                         <form id="contact-form"  action="Tabla.php"  method="Post">
                                            <div class="form-group mb-4">
                                                <label> (todavía no funciona) hablar a mi Discord (ganamoss#5348) </label>
                                                <input type="text" name="player" id="player" class="form-control" placeholder="nombre"  />
                                                <input type="text" name="twitch" class="form-control" placeholder="Canal De twitch"  />
                                            </div>
                                            <div class="text-right">
                                                <a href="Tabla.php">Iniciar como invitado</a>
                                                <button type="submit" class="btn btn-big btn-primary">Send It</button>
                                                
                                            </div>
                                       </form>
                        
            </div>
        </div>        

        <div class="tm-row">
            <div class="tm-col-left text-center">            
                <ul class="tm-bg-controls-wrapper">
                    <li class="tm-bg-control active" data-id="0"></li>
                    <li class="tm-bg-control" data-id="1"></li>
                    <li class="tm-bg-control" data-id="2"></li>
                </ul>            
            </div>        
            <div class="tm-col-right tm-col-footer">
                <footer class="tm-site-footer text-right">
                    <p class="mb-0">Copyright: Comander Borja | Discord: ganamoss#5348</p>
                </footer>
            </div>  
        </div>

       
        <div class="tm-bg">
            <div class="tm-bg-left"></div>
            <div class="tm-bg-right"></div>
        </div>
    </div>

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.backstretch.min.js"></script>
    <script src="js/templatemo-script.js"></script>
</body>
</html>

    
</body>
</html>